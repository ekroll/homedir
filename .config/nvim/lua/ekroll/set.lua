function set(vim)
    vim.opt.termguicolors = true

    vim.opt.nu = true
    vim.opt.relativenumber = true
    vim.opt.colorcolumn = "80"

    vim.opt.signcolumn = "yes"
    vim.opt.tabpagemax = 9
    vim.opt.splitright = true
    vim.opt.splitbelow = true

    vim.opt.tabstop = 4
    vim.opt.shiftwidth = 4
    vim.opt.softtabstop = 4
    vim.opt.expandtab = true
    vim.opt.wrap = false

    -- Make needs real tabs
    vim.api.nvim_create_autocmd( "FileType", {
        pattern = "make",
        callback = function()
            vim.opt_local.expandtab = false
            vim.opt_local.list = true
        end
    })

    vim.opt.backup = false
    vim.opt.swapfile = false
    vim.opt.undofile = true
    vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"

    vim.opt.hlsearch = false
    vim.opt.incsearch = true

    --vim.opt.scrolloff = 8
    vim.opt.updatetime = 50

    vim.g.vimwiki_markdown_link_ext = 0
    vim.g.vimwiki_list = {{
        syntax = 'markdown',
        ext = '.md'
    }}
end

set(vim)
