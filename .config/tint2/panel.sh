#!/usr/bin/bash
if [[ -z "$(pgrep tint2)" ]]; then
    (sleep 0.1s && G_SLICE=always-malloc tint2 -c ~/.config/tint2/tint2rc-left) &
    (sleep 0.2s && G_SLICE=always-malloc tint2 -c ~/.config/tint2/tint2rc-center) &
    (sleep 0.3s && G_SLICE=always-malloc tint2 -c ~/.config/tint2/tint2rc-right) &
else
    kill $(pgrep -a tint2 | grep left   | awk '{print $1}') && sleep 0.1s
    kill $(pgrep -a tint2 | grep center | awk '{print $1}') && sleep 0.2s
    kill $(pgrep -a tint2 | grep right  | awk '{print $1}') && sleep 1.3s
fi
