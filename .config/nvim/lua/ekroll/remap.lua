function remap(vim)
    -- Foundational
    vim.g.mapleader = " "
    vim.keymap.set("i", "jj", "<esc>")
    vim.keymap.set("n", "<leader>pv", ":Ex<CR>")
    vim.keymap.set("n", "<leader>lw", ":set wrap<CR>")
    vim.keymap.set("n", "<leader>le", ":set nowrap<CR>")

    -- Centered Movement
    vim.keymap.set("n", "<C-d>", "<C-d>zz") -- moving (up/down) in half pages
    vim.keymap.set("n", "<C-u>", "<C-u>zz")
    vim.keymap.set("n", "j", "gjzz")        -- moving (up/down) in lines
    vim.keymap.set("n", "k", "gkzz")

    vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv") -- moving selection (up/down) in lines
    vim.keymap.set("v", "K", ":m '<-1<CR>gv=gv")

    vim.keymap.set("n", "n", "nzzzv") -- next/previous search result
    vim.keymap.set("n", "N", "Nzzzv")

    -- change tabs
    vim.keymap.set('n', '<C-Tab>', ':tabnext<CR>')
    vim.keymap.set('n', '<C-S-Tab>', ':tabprev<CR>')


    -- h and l to navigate netwr
    vim.api.nvim_create_autocmd( "FileType", {
        pattern = "netrw",
        callback = function()
            vim.keymap.set("n", "h", "-<esc>", { noremap = false, silent = true })
            vim.keymap.set("n", "l", "<CR>", { noremap = false, silent = true })
        end
    })


    -- Sane register / clipboard interplay
    vim.keymap.set("n", "<leader>y", "\"+y")
    vim.keymap.set("n", "<leader>Y", "\"+Y")
    vim.keymap.set("v", "<leader>y", "\"+y")
    vim.keymap.set("x", "<leader>p", "\"_dP")

    -- tmux-sessionizer
    vim.keymap.set("n", "<C-f>", "<cmd>silent !tmux neww tmux-sessionizer<CR>")

    -- Fix executable permissions for current file
    vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

    -- Compile % to PDF and view with pandoc
    --vim.keymap.set("n", "<leader>z", "<cmd>!pandoc -o \"%.pdf\" \"%\" --pdf-engine=xelatex; zathura \"%.pdf\" &<CR>")
    vim.keymap.set("n", "<leader>z", "<cmd>!pandoc -o \"%.pdf\" \"%\" --mathjax; zathura \"%.pdf\" &<CR>")
end

remap(vim)
