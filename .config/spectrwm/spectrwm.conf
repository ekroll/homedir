# BEHAVIOUR
workspace_limit	= 9 # I don't need 22 workspaces
warp_focus = 1 # move focus when changing workspace
focus_default = first # focus master window by default (lets say ws-change)
focus_close	= previous # focus previous window when closing a window
focus_close_wrap = 0 # NO TO FOCUS UNDER/OVERFLOW WHEN CLOSING LAST OR MASTER
warp_pointer = 1 # warp pointer when focus changes via keyboard
focus_mode = follow # otherwise use pointer LOCATION (not click) to focus windows
focus_mode[map,unmap] = follow # don't warp pointer when opening/closing windows
spawn_position = next # my preferred way to make new windows not occupy master
workspace_autorotate = 1 # rotation follows xrandr
workspace_clamp	= 0 # swap workspaces instead of focusing them (on other monitors)
cycle_visible = 1 # left-right movement cycles visible workspaces
layout_order = vertical,max # the layouts I need (prior dwm user)


# AUTOSTART
autorun = ws[1]:autolock
autorun = ws[1]:autorandr -c
autorun = ws[1]:picom
autorun = ws[1]:hsetroot
autorun = ws[1]:redshift
autorun = ws[1]:pasystray
autorun = ws[1]:nm-applet
autorun = ws[1]:flameshot
autorun = ws[1]:dunst
autorun = ws[1]:~/.config/tint2/panel.sh
autorun	= ws[1]:brave
autorun	= ws[2]:st -e tmux
autorun = ws[4]:signal-desktop


# BUILTIN PROGRAMS
program[lock]		= lock
program[term]		= st
program[menu]		= rofi -show combi -modes combi -combi-modes "window,drun,run" -show-icons
program[search]	= dmenu $dmenu_bottom -i -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected
program[screenshot_all]	= flameshot gui
#program[screenshot_wind]	= screenshot.sh window	# optional
#program[initscr]	= initscreen.sh			# optional


# CUSTOM PROGRAMS (DEFINED)
program[newbg]       = newbg
program[toggle_bar]  = ~/.config/tint2/panel.sh
program[taskmanager] = sysmontask
program[explorer]    = nautilus
program[explorer2]   = st -c "explorer" -e lf
program[volume]      = pavucontrol
program[crownest]    = patchance


# KEYBOARD BINDINGS
modkey = Mod4
keyboard_mapping  = ~/.config/spectrwm/spectrwm_us.conf
bind[newbg]       = MOD+w
bind[toggle_bar]  = MOD+b # This is my second bar, swm one is always visible
bind[taskmanager] = Control+Shift+Escape
bind[explorer]    = MOD+e
bind[explorer2]   = MOD+Shift+e
bind[volume]      = MOD+v
bind[crownest]    = MOD+c


# QUIRKS (WM RULES)
quirk[:Error]             = FLOAT
quirk[org.gnome.Nautilus] = FLOAT
quirk[explorer]           = FLOAT
quirk[pavucontrol]        = FLOAT
quirk[Patchance]          = FLOAT
quirk[Code]               = WS[3]
quirk[Godot]              = WS[3]
quirk[Signal]             = WS[4]
quirk[Slack]              = WS[5]
quirk[Blender]            = WS[6]
quirk[TeamViewer]         = WS[9]


# Window Decoration
border_width                 = 3
tile_gap                     = 12
region_padding               = 12
color_focus		             = cyan
color_focus_free	         = yellow
color_focus_maximized	     = magenta
color_focus_maximized_free	 = yellow
color_unfocus	             = rgb:00/55/55
color_unfocus_free	         = rgb:55/55/00
color_unfocus_maximized		 = rgb:55/00/55
color_unfocus_maximized_free = rgb:55/55/00
disable_border		         = 0 # Remove window border when bar is disabled and there is only one window in workspace


# Bar Settings
bar_enabled		= 1
bar_enabled_ws[1]	= 1
bar_border_width	= 3
bar_border[1]		= rgb:00/00/00/00
bar_border_unfocus[1]	= rgb:00/00/00/55
bar_border_free[1]	= black
bar_color[1]		= black
bar_color_unfocus[1]	= black
bar_color_free[1]	= black
bar_color_selected[1]	= rgb:00/80/80
bar_font_color[1]	= cyan, rgb:00/00/55, cyan, cyan, white
bar_font_color_unfocus[1]	= rgb:00/80/80
bar_font_color_free[1]	= yellow
bar_font_color_selected	= cyan
bar_font		= xos4 Terminus:pixelsize=14:antialias=true
bar_font_pua		= Typicons:pixelsize=14:antialias=true
bar_action_expand	= 1
bar_justify		= left
bar_format		= +|L +S +C +|C +F +W (+M Minimized Windows) +|R %R %Z
workspace_indicator	= listcurrent,listactive,markcurrent,printnames
workspace_mark_current	= '['
workspace_mark_current_suffix	= ']'
workspace_mark_active	= '^'
workspace_mark_active_suffix	= ''
workspace_mark_empty	= '-'
workspace_mark_empty_suffix	= ''
workspace_mark_urgent	= '!'
workspace_mark_urgent_suffix	= ''
bar_at_bottom		= 0
stack_enabled		= 1
stack_mark_horizontal	= '[-]'
stack_mark_horizontal_flip	= '[v]'
stack_mark_vertical	= '[|]'
stack_mark_vertical_flip	= '[>]'
stack_mark_max		= '[ ]'
stack_mark_floating	= '[~]'
focus_mark_none	= ''
focus_mark_normal	= ''
focus_mark_floating	= '(f)'
focus_mark_maximized	= '(m)'
focus_mark_free	= '(*)'
iconic_enabled		= 1
fullscreen_hide_other	= 1
maximize_hide_bar	= 1
maximize_hide_other	= 1
window_class_enabled	= 1
window_instance_enabled	= 1
window_name_enabled	= 0
verbose_layout		= 1
urgent_enabled		= 1
urgent_collapse	= 0

# Split a non-RandR dual head setup into one region per monitor
# (non-standard driver-based multihead is not seen by spectrwm)
#region			= screen[1]:1280x1024+0+0
#region			= screen[1]:1280x1024+1280+0

# Customize workspace layout at start
#layout			= ws[1]:4:0:0:0:vertical
#layout			= ws[2]:0:0:0:0:vertical
#layout			= ws[3]:0:0:0:0:max
#layout			= ws[4]:4:0:0:0:vertical_flip
#layout			= ws[5]:0:0:0:0:max
#layout			= ws[6]:0:0:0:0:floating

# Set workspace name at start
#name			= ws[1]:Web Browsing
#name			= ws[2]:Terminal
#name			= ws[3]:IDE's
#name			= ws[5]:Electron Bloat

# Region containment
# Distance window must be dragged/resized beyond the region edge before it is
# allowed outside the region.
boundary_width 		= 50

# Dialog box size ratio when using TRANSSZ quirk; 0.3 < dialog_ratio <= 1.0
dialog_ratio		= 0.6
