# Patriarch Linux

![desktop](readme/desktop.png)

Arch-rice tailored for Orthodox Christians featuring:

* SpectrWM (dynamic tiler)
* Tint2 (light status panel)
* Rofi (spotlight/startmenu)
* Picom (X11 Compositor)
* PipeWire (Audio server abstraction managing ALSA. PulseAudio and JACK)
* i3lock-color (patched lockscreen & homemade wallpaper)
* Various scripts (cli daily readings++)

Expecting:

* That you have set up your system [this way](https://github.com/ingar195/Arch)
* that you will be managing dotfiles with git
* That you do your duty as a fanatic linux evangelist

Strongly considering *OrthoBSD* for my next endeavour (but my friend is telling me I need to use NixOS)
